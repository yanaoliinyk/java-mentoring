package com.epam.training.domain;

import com.epam.training.exceptions.ValidationException;

import java.util.Arrays;

public enum Outcome {
    FIRST_TEAM_WINS("1"),
    SECOND_TEAM_WINS("2"),
    DRAW("X");

    private final String value;

    Outcome(String value) {
        this.value = value;
    }

    public static Outcome getOutcome(String value) throws ValidationException {
        return Arrays.stream(values())
                .filter(outcome -> value.toLowerCase().equals(outcome.value.toLowerCase()))
                .findFirst().orElseThrow(() -> new ValidationException(value + " was not present"));
    }
}
