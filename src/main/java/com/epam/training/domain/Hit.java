package com.epam.training.domain;

public class Hit {

    private int hits;
    private int gamesNumber;
    private int prize;

    public Hit(int hits, int gamesNumber, int prize) {
        this.hits = hits;
        this.gamesNumber = gamesNumber;
        this.prize = prize;
    }

    public int getHits() {
        return hits;
    }

    public int getPrize() {
        return prize;
    }

    @Override
    public String toString() {
        return "Hit{" +
                hits + ") " +
                "gamesNumber=" + gamesNumber +
                ", prize=" + prize +
                '}';
    }
}
