package com.epam.training.domain;

import com.epam.training.exceptions.ValidationException;
import com.epam.training.utils.ValidationHelper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Round {
    private int year;
    private int week;
    private int round;
    private LocalDate date;
    private Hit[] hits;
    private List<Outcome> outcomes = new ArrayList<>();

    private Round() {
    }

    private void setYear(int year) {
        this.year = year;
    }

    private void setWeek(int week) {
        this.week = week;
    }

    private void setRound(int round) {
        this.round = round;
    }

    public LocalDate getDate() {
        return date;
    }

    private void setDate(LocalDate date) {
        this.date = date;
    }

    public Hit[] getHits() {
        return hits;
    }

    private void setHits(Hit[] hits) {
        this.hits = hits;
    }

    public List<Outcome> getOutcomes() {
        return outcomes;
    }

    private void setOutcomes(List<Outcome> outcomes) {
        this.outcomes = outcomes;
    }

    @Override
    public String toString() {
        return "Round{" +
                "year=" + year +
                ", week=" + week +
                ", round=" + round +
                ", date=" + date +
                ", hits=" + Arrays.asList(hits) +
                ", outcomes=" + outcomes +
                '}';
    }

    public static class Builder {
        private int year;
        private int week;
        private int roundNumber;
        private LocalDate date;
        private Hit[] hits;
        private List<Outcome> outcomes;

        public Builder atYear(String year) {
            this.year = Integer.parseInt(year);
            return this;
        }

        public Builder atWeek(String week) {
            this.week = Integer.parseInt(week);
            return this;
        }

        public Builder withRoundNumber(String round) {
            try {
                this.roundNumber = Integer.parseInt(round);
            } catch (NumberFormatException e) {
                this.roundNumber = 0;
            }
            return this;
        }

        public Builder atDate(String date, String dateFormat) {
            try {
                this.date = ValidationHelper.getDateFromString(date, dateFormat);
            } catch (ValidationException e) {
                this.date = null;
            }
            return this;
        }

        public Builder withHits(String[] hits) throws ValidationException {
            this.hits = ValidationHelper.getHits(hits);
            return this;
        }

        public Builder withOutcome(String[] outcomes) throws ValidationException {
            this.outcomes = ValidationHelper.getOutcomesFromString(outcomes);
            return this;
        }

        public Round build() {
            Round round = new Round();
            round.setYear(year);
            round.setWeek(week);
            round.setRound(roundNumber);
            round.setDate(date);
            round.setHits(hits);
            round.setOutcomes(outcomes);

            return round;
        }
    }
}
