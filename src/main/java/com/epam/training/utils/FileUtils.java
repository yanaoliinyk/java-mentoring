package com.epam.training.utils;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

class FileUtils {

    static List<String[]> parseCSV(final String csvFile) {
        List<String[]> lineValues = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(csvFile))) {
            stream.forEach(l -> lineValues.add(l.split(";")));
        } catch (Exception e) {
            System.out.println("error while reading file");
        }
        return lineValues;
    }
}
