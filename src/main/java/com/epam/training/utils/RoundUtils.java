package com.epam.training.utils;

import com.epam.training.domain.Round;
import com.epam.training.exceptions.ValidationException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RoundUtils {

    private static final String CSV_DATE_PATTERN = "yyyy.MM.dd.";

    public static List<Round> getRounds(String csvFileName) throws ValidationException {
        List<Round> rounds = new ArrayList<>();
        List<String[]> fileLines = FileUtils.parseCSV(csvFileName);
        if (fileLines.isEmpty()) {
            throw new ValidationException("file was empty");
        }
        for (String[] fileLine : fileLines) {
            Round round = new Round.Builder()
                    .atYear(fileLine[0])
                    .atWeek(fileLine[1])
                    .withRoundNumber(fileLine[2])
                    .atDate(fileLine[3], CSV_DATE_PATTERN)
                    .withHits(Arrays.copyOfRange(fileLine, 4, 14))
                    .withOutcome(Arrays.copyOfRange(fileLine,14, fileLine.length))
                    .build();
            rounds.add(round);
        }
        return rounds;
    }

    public static Round getRoundForDate(List<Round> rounds, LocalDate date) throws ValidationException {
        return rounds.stream()
                .filter(r -> date.equals(r.getDate()))
                .findFirst().orElseThrow(() -> new ValidationException("No round for date found"));
    }
}
