package com.epam.training.utils;

import com.epam.training.domain.Hit;
import com.epam.training.domain.Outcome;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.epam.training.exceptions.ValidationException;

public class ValidationHelper {
    private static final int NUMBER_OF_OUTCOMES = 14;

    public static LocalDate getDateFromString(String date, String pattern) throws ValidationException {
        try {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
            return dateTimeFormatter.parse(date, LocalDate::from);
        } catch (DateTimeParseException e) {
            throw new ValidationException("Date was incorrect: " + date);
        }
    }

    public static List<Outcome> getOutcomesFromString(String outcomesLine) throws ValidationException {
        if (outcomesLine.trim().length() != NUMBER_OF_OUTCOMES) {
            throw new ValidationException("incorrect number of outcomes: "+ outcomesLine);
        }
        List<Outcome> outcomes = new ArrayList<>();
        for (int i = 0; i < outcomesLine.length(); i++) {
            outcomes.add(Outcome.getOutcome(getClearOutcome(String.valueOf(outcomesLine.charAt(i)))));
        }
        return outcomes;
    }

    public static List<Outcome> getOutcomesFromString(String[] outcomes) throws ValidationException {
        List<Outcome> outcomesList = new ArrayList<>();
        for (String outcome : outcomes) {
            outcomesList.add(Outcome.getOutcome(getClearOutcome(outcome)));
        }
        return outcomesList;
    }

    private static String getClearOutcome(String outcome) throws ValidationException {
        String clearOutcome = outcome.replaceAll("[^(12xX)]", "");
        if (clearOutcome.isEmpty()) {
            throw new ValidationException("one of outcomes was incorrect formatted");
        } else {
            return clearOutcome;
        }
    }

    public static Hit[] getHits(String[] values) throws ValidationException {
        Hit[] hits = new Hit[values.length / 2];
        for (int i = 0; i < hits.length; i++) {
            hits[i] = getHit(14 - i, values[2 * i], values[2 * i + 1]);
        }
        return hits;
    }

    private static Hit getHit(int hits, String gamesNumber, String prize) throws ValidationException {
        Hit hit;
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols(Locale.ENGLISH);
        formatSymbols.setGroupingSeparator(' ');
        DecimalFormat format = new DecimalFormat("###,###", formatSymbols);
        try {
            hit = new Hit(hits, Integer.parseInt(gamesNumber), format.parse(prize).intValue());
        } catch (ParseException e) {
            throw new ValidationException("Could not get hit");
        }
        return hit;
    }

}
