package com.epam.training.utils;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class MenuUtils {

    public static Options prepareOptions(){
        Options options = new Options();

        Option helpOption = new Option("h", "help", false, "show help");
        Option maxOption = new Option("max","maxprize", false, "finds max prize ever");
        Option distributionOption = new Option("dstr", "distribution",
                true, "finds wins distribution for date");
        distributionOption.setArgs(1);
        distributionOption.setArgName("date");
        Option prizeOption = new Option("pr", "prize", true,
                "calculates prize for specified date and outcomes");
        prizeOption.setArgs(2);
        prizeOption.setArgName("args");

        options.addOption(maxOption).addOption(distributionOption).addOption(helpOption).addOption(prizeOption);
        return options;
    }
}
