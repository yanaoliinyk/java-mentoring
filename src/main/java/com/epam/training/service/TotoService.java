package com.epam.training.service;

import com.epam.training.domain.Hit;
import com.epam.training.domain.Outcome;
import com.epam.training.domain.Round;
import com.epam.training.exceptions.ValidationException;
import com.epam.training.utils.RoundUtils;
import com.epam.training.utils.ValidationHelper;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.LocalDate;
import java.util.*;

@Component
public class TotoService {

    private static final String DATE_PATTERN = "yyyy.MM.dd";

    public int getLargestPrize(List<Round> rounds) throws ValidationException {
        return rounds.stream()
                .map(r -> Arrays.stream(r.getHits())
                        .mapToInt(Hit::getPrize)
                        .max())
                .mapToInt(OptionalInt::getAsInt)
                .max().orElseThrow(() -> new ValidationException("could not find max prize"));
    }

    public String getRoundDistribution(String dateString, List<Round> rounds) throws ValidationException {
        LocalDate date = ValidationHelper.getDateFromString(dateString, DATE_PATTERN);
        Round round = RoundUtils.getRoundForDate(rounds, date);
        return getRoundDistribution(round);
    }

    private String getRoundDistribution(Round round) {

        int firstWins = (int) round.getOutcomes().stream().filter(o -> o.equals(Outcome.FIRST_TEAM_WINS)).count();
        int secondWins = (int) round.getOutcomes().stream().filter(o -> o.equals(Outcome.SECOND_TEAM_WINS)).count();
        int draw = (int) round.getOutcomes().stream().filter(o -> o.equals(Outcome.DRAW)).count();

        int sum = firstWins + secondWins + draw;

        DecimalFormat format = new DecimalFormat(" #,##0.00 %");

        return String.format("team #1 won: %s team #2 won: %s draw: %s",
                format.format((double) firstWins / sum), format.format((double) secondWins / sum),
                format.format((double) draw / sum));
    }

    public String calculatePrize(List<Round> rounds, String date, String outcomes) throws ValidationException {
        List<Outcome> outcomesList = ValidationHelper.getOutcomesFromString(outcomes);

        Round round = RoundUtils.getRoundForDate(rounds, ValidationHelper.getDateFromString(date, DATE_PATTERN));

        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols(Locale.ENGLISH);
        formatSymbols.setGroupingSeparator(' ');
        DecimalFormat format = new DecimalFormat("###,### 'UAH'", formatSymbols);
        try {
            return format.format(calculatePrize(round, outcomesList));
        } catch (NoSuchFieldException e) {
            System.out.println("unable to calculate prize");
            return null;
        }
    }


    private int calculatePrize(Round round, List<Outcome> outcomes) throws NoSuchFieldException {
        List<Outcome> realOutcomes = round.getOutcomes();
        int matchCounter = 0;

        for (int i = 0; i < realOutcomes.size(); i++) {
            if (outcomes.get(i).equals(realOutcomes.get(i))) {
                matchCounter++;
            }
        }
        if (matchCounter < 10)
            return 0;
        else {
            int finalMatchCounter = matchCounter;
            return Arrays.stream(round.getHits())
                    .filter(h -> finalMatchCounter == h.getHits())
                    .findFirst()
                    .orElseThrow(NoSuchFieldException::new)
                    .getPrize();
        }
    }
}
