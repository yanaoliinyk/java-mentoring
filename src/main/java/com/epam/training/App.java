package com.epam.training;

import com.epam.training.domain.Round;
import com.epam.training.exceptions.ValidationException;
import com.epam.training.service.TotoService;
import com.epam.training.utils.RoundUtils;
import org.apache.commons.cli.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

import static com.epam.training.utils.MenuUtils.prepareOptions;

@SpringBootApplication
public class App implements CommandLineRunner {

    private TotoService totoService;

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Autowired
    public App(TotoService totoService){
        this.totoService = totoService;
    }

    @Override
    public void run(String... args) {
        try {
            Options options = prepareOptions();

            CommandLineParser parser = new DefaultParser();
            CommandLine commandLine = parser.parse(options, args);

            menu(commandLine, options);
        } catch (ParseException e) {
            System.out.println("incorrect params entered");
        }
    }

    private void menu(CommandLine commandLine, Options options) {
        List<Round> rounds;
        try {
            rounds = RoundUtils.getRounds("src/main/resources/toto.csv");

            System.out.println("APPLICATION STARTS");
            if (commandLine.hasOption("h")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("CommandLineParameters", options);
            }
            else if (commandLine.hasOption("max")) {
                showLargestPrizeEver(rounds);
            }
            else if (commandLine.hasOption("dstr")) {
                showDistribution(rounds, commandLine.getOptionValues("dstr")[0]);
            }
            else if (commandLine.hasOption("pr")) {
                String[] args = commandLine.getOptionValues("pr");
                if (args.length != 2) {
                    throw new ValidationException("incorrect number of args");
                }
                showMyPrize(rounds, args[0], args[1]);
            }
        } catch (ValidationException e) {
            System.out.println(e.getMessage());
        }
    }

    private void showLargestPrizeEver(List<Round> rounds) throws ValidationException {
        System.out.println("Largest prize ever: " + totoService.getLargestPrize(rounds));
    }

    private void showDistribution(List<Round> rounds, String date) throws ValidationException {
        System.out.println(totoService.getRoundDistribution(date, rounds));
    }

    private void showMyPrize(List<Round> rounds, String date, String outcomes) throws ValidationException {
        System.out.println("You won " + totoService.calculatePrize(rounds, date, outcomes));
    }
}
